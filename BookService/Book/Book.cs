﻿using System;

namespace Book
{
    public class Book : IEquatable<Book>, IComparable<Book>
    {
        private bool _published;
        private DateTime _datePublished;
        private int _totalPages;

        public string Author { get; }
        public string Title { get; }
        public string Publisher { get; }
        public string ISBN { get; }
        public decimal Price { get; private set; }
        public string Currency { get; private set; }

        public int Pages
        {
            get => _totalPages;
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException(nameof(value));
                _totalPages = value;
            }
        }

        public Book(string author, string title, string publisher)
        {
            Author = author;
            Title = title;
            Publisher = publisher;
        }

        public Book(string author, string title, string publisher, string isbn)
            : this(author, title, publisher)
        {
            ISBN = isbn;
        }

        public void Publish(DateTime dateTime)
        {
            _published = true;
            _datePublished = dateTime;
        }

        public void SetPrice(decimal price, string currency)
        {
            if (price < 0) throw new ArgumentException(nameof(Price));
            Currency = currency;
            Price = price;
        }

        public string GetPublicationDate()
        {
            return _published ? _datePublished.ToString("MM/dd/yyyy") : "NYP";
        }

        public override string ToString()
        {
            return $"{Title} by {Author}";
        }

        public int CompareTo(Book other)
        {
            if (other == null) return -1;
            if (Title.Equals(other.Title)) return 0;
            if (Title.Length < other.Title.Length) return -1;
            return -1;
        }

        public override bool Equals(object? other)
        {
            return ISBN == ((Book) other)?.ISBN;
        }

        public bool Equals(Book other)
        {
            return ISBN == other?.ISBN;
        }
        
        public override int GetHashCode()
        {
            return _published
                ? (Publisher.Length + 3) * (Author.Length + Title.Length + 5)
                : (Math.Abs(Author.Length - Title.Length) + 7) *
                  (Author.Length + Title.Length + 11) +
                  Publisher.Length;
        }

    }
}