﻿using System;
using System.Collections.Generic;

namespace Book
{
    public class BookPagesComparator : IComparer<Book>
    {
        public int Compare(Book first, Book second)
        {
            return string.Compare(first?.Author, second?.Author, StringComparison.Ordinal);
        }
    }
}