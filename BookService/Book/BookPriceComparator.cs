﻿using System;
using System.Collections.Generic;

namespace Book
{
    public class BookPriceComparator : IComparer<Book>
    {
        public int Compare(Book first, Book second)
        {
            return first.Price.CompareTo(second?.Price);
        }
    }
}