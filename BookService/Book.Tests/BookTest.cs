using System;
using NUnit.Framework;
using Book;

namespace Book.Tests
{
    public class Tests
    {
        private Book _book;
        
        [SetUp]
        public void SetUp()
        {
            _book = new Book("Alexander Pushkin","Dubrovskiy","Moskva");
        }
        
        [TestCase(1841,9,10)]
        [TestCase(2013,12,14)]
        public void TestPublish(int publishYear, int publishMonth, int publishDay)
        {
            var date = new DateTime(publishYear, publishMonth, publishDay);
            _book.Publish(date);
            Assert.AreEqual(_book.GetPublicationDate(), $"{publishMonth:D2}/{publishDay:D2}/{publishYear:D4}");
        }

        [TestCase(9.99,"USD")]
        [TestCase(25.06,"BYN")]
        public void TestSetPrice(decimal price, string currency)
        {
            _book.SetPrice(price,currency);
            Assert.IsTrue(price == _book.Price && currency == _book.Currency);
        }
    }
}