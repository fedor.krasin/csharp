using System;
using Book;
using NUnit.Framework;

namespace BookListService.Tests
{
    public class BookListServiceTests
    {
        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications")]
        public void BookServiceAddBookTest(string author, string title, string publisher)
        {
            var bookListService = new BookListService();
            var book = new Book.Book(author, title, publisher);

            bookListService.Add(book);
            Assert.IsFalse(bookListService.IsEmpty());
        }

        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications")]
        public void BookServiceAddSameBookTest(string author, string title, string publisher)
        {
            var bookListService = new BookListService();
            var book = new Book.Book(author, title, publisher);

            bookListService.Add(book);
            Assert.Throws<Exception>(() => bookListService.Add(book), "This book is already stored.");
        }

        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications")]
        public void BookServiceRemoveBookTest(string author, string title, string publisher)
        {
            var bookListService = new BookListService();
            var book = new Book.Book(author, title, publisher);
            
            bookListService.Add(book);
            bookListService.Remove(book);
            Assert.IsTrue(bookListService.IsEmpty());
        }

        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications")]
        public void BookServiceRemoveNotExistingBook(string author, string title, string publisher)
        {
            var bookListService = new BookListService();
            var book = new Book.Book(author, title, publisher);
            bookListService.Add(new Book.Book(string.Empty, string.Empty, string.Empty));

            Assert.Throws<Exception>(() => bookListService.Remove(book), "Failed to remove the book. The book is out of storage.");
        }

        [TestCase("Jon Skeet")]
        public void BookServiceFindByAuthorTest(string author)
        {
            var bookListService = new BookListService();

            var book1 = new Book.Book(string.Empty, string.Empty, "Manning Publications");
            var book2 = new Book.Book(author, string.Empty, string.Empty);
            var book3 = new Book.Book(string.Empty, "C# in Depth", string.Empty);
            var book4 = new Book.Book(string.Empty, string.Empty, string.Empty);
            var book5 = new Book.Book(author, "C# in Depth", string.Empty);

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);
            bookListService.Add(book4);
            bookListService.Add(book5);

            var list = bookListService.FindBy(book => book.Author.Equals(author));

            Assert.IsTrue(list.Contains(book2) && list.Contains(book5) && list.Count == 2);
        }

        [TestCase("C# in Depth")]
        public void BookServiceFindByTitleTest(string title)
        {
            var bookListService = new BookListService();

            var book1 = new Book.Book(string.Empty, string.Empty, "Manning Publications");
            var book2 = new Book.Book("Jon Skeet", string.Empty, string.Empty);
            var book3 = new Book.Book(string.Empty, title, string.Empty);
            var book4 = new Book.Book(string.Empty, string.Empty, string.Empty);
            var book5 = new Book.Book("Jon Skeet", title, string.Empty);

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);
            bookListService.Add(book4);
            bookListService.Add(book5);

            var list = bookListService.FindBy(book => book.Title.Equals(title));

            Assert.IsTrue(list.Contains(book3) && list.Contains(book5) && list.Count == 2);
        }

        [TestCase("Manning Publications")]
        public void BookServiceFindByPublisherTest(string publisher)
        {
            var bookListService = new BookListService();

            var book1 = new Book.Book(string.Empty, string.Empty, publisher);
            var book2 = new Book.Book("Jon Skeet", string.Empty, string.Empty);
            var book3 = new Book.Book(string.Empty, "C# in Depth", string.Empty);
            var book4 = new Book.Book(string.Empty, string.Empty, string.Empty);
            var book5 = new Book.Book("Jon Skeet", "C# in Depth", string.Empty);

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);
            bookListService.Add(book4);
            bookListService.Add(book5);

            var list = bookListService.FindBy(book => book.Publisher.Equals(publisher));

            Assert.IsTrue(list.Contains(book1) && list.Count == 1);
        }

        [TestCase("C", "A", "B")]
        public void BookServiceGetByAuthorTest(string author1, string author2, string author3)
        {
            var bookListService = new BookListService();

            var book1 = new Book.Book(author1, string.Empty, string.Empty);
            var book2 = new Book.Book(author2, string.Empty, string.Empty);
            var book3 = new Book.Book(author3, string.Empty, string.Empty);
   
            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);

            var list = bookListService.GetBy(new BookAuthorComparator());

            Assert.IsTrue(list[0].Equals(book2) && list[1].Equals(book3) && list[2].Equals(book1));
        }

        [TestCase(300, 40, 230)]
        public void BookServiceGetByPagesTest(int pages1, int pages2, int pages3)
        {
            var bookListService = new BookListService();

            var book1 = new Book.Book("A", string.Empty, string.Empty) { Pages = pages1 };
            var book2 = new Book.Book("B", string.Empty, string.Empty) { Pages = pages2 };
            var book3 = new Book.Book("C", string.Empty, string.Empty) { Pages = pages3 };

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);

            var list = bookListService.GetBy(new BookPagesComparator());

            Assert.IsTrue(list[0].Equals(book2) && list[1].Equals(book3) && list[2].Equals(book1));
        }

        [TestCase(300, 40, 230, "USD")]
        public void BookServiceGetByPriceTest(int price1, int price2, int price3, string currency)
        {
            var bookListService = new BookListService();

            var book1 = new Book.Book("A", string.Empty, string.Empty);
            var book2 = new Book.Book("B", string.Empty, string.Empty);
            var book3 = new Book.Book("C", string.Empty, string.Empty);

            book1.SetPrice(price1, currency);
            book2.SetPrice(price2, currency);
            book3.SetPrice(price3, currency);

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);

            var list = bookListService.GetBy(new BookPriceComparator());

            Assert.IsTrue(list[0].Equals(book2) && list[1].Equals(book3) && list[2].Equals(book1));
        }
    }
}