﻿using System;
using System.Collections.Generic;
using Book;

namespace BookListService
{
    public class BookListService
    {
        private List<Book.Book> _bookList = new List<Book.Book>();

        public List<Book.Book> GetBookList()
        {
            return _bookList;
        }

        public void Add(Book.Book book)
        {
            if (!_bookList.Contains(book))
            {
                _bookList.Add(book);
            } 
            else
            {
                throw new Exception("This book has already been added.");
            }
        }
        
        public void Remove(Book.Book book)
        {
            if (!_bookList.Contains(book))
            {
                _bookList.Remove(book);
            } 
            else
            {
                throw new Exception("There is no such book.");
            }
        }

        public List<Book.Book> FindBy(Predicate<Book.Book> predicate)
        {
            return _bookList.FindAll(predicate);
        }

        public List<Book.Book> GetBy(IComparer<Book.Book> comparer)
        {
            _bookList.Sort(comparer);
            return _bookList;
        }
        
        public bool IsEmpty()
        {
            return _bookList.Count == 0;
        }

        public void Load(BookStorage bookStorage)
        {
            if (_bookList.Count == 0)
            {
                var books = bookStorage.GetBooks();

                foreach (var book in books)
                {
                    _bookList.Add(book);
                }
            }
            else
            {
                var books = bookStorage.GetBooks();

                foreach (var book in books)
                {
                    _bookList.Add(book);
                }
            }
        }
        
        public void Save(BookStorage bookStorage) 
        {
            var books = new List<Book.Book>();

            foreach (var book in _bookList)
            {
                books.Add(book);
            }

            bookStorage.AddBooks(books);
        }
    }
}