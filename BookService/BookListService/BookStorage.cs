﻿using System.Collections.Generic;

namespace BookListService
{
    public class BookStorage
    {
        private List<Book.Book> _books;
        
        public BookStorage()
        {
            _books = new List<Book.Book>();
        }

        public BookStorage(List<Book.Book> books)
        {
            _books = books;
        }

        public List<Book.Book> GetBooks()
        {
            return _books;
        }

        public void AddBooks(List<Book.Book> books)
        {
            foreach (var book in books)
            {
                _books.Add(book);
            }
        }
        
        public bool IsEmpty()
        {
            if (_books.Count == 0) return true;
            return false;
        }
    }
}